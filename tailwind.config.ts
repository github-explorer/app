const colors = require('tailwindcss/colors');

export const configs = {
  content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  theme: {
    extend: {
      colors: {
        gray: colors.slate,
        'light-blue': colors.sky,
        teal: colors.teal,
        black: colors.black,
        red: colors.rose,
      }
    },
  },
  plugins: [],
};

export default configs;