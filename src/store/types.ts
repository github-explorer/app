import { SearchStoreType } from 'src/pages/Search/types';
import { RepositoryStoreType } from 'src/pages/Repository/types';

export type Store = {
  repository: RepositoryStoreType;
  search: SearchStoreType;
};
