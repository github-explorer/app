import repository from 'src/pages/Repository/reducer';
import search from 'src/pages/Search/reducer';

export const reducers = {
  repository,
  search,
};

export default reducers;
