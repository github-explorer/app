import { takeEvery } from 'redux-saga/effects';

import { SEARCH as searchActionTypes } from 'src/pages/Search/types';
import * as searchSagas from 'src/pages/Search/sagas';

import { REPOSITORY as repositoryActionTypes } from 'src/pages/Repository/types';
import * as repositorySagas from 'src/pages/Repository/sagas';

function* sagas() {
  yield takeEvery(
    searchActionTypes.FETCH_DATA,
    searchSagas.handleSearchRepositories,
  );

  yield takeEvery(
    repositoryActionTypes.FETCH_DATA,
    repositorySagas.handleFetchRepository,
  );
  yield takeEvery(
    repositoryActionTypes.FETCH_README_DATA,
    repositorySagas.handleFetchRepositoryReadme,
  );
}

export default sagas;
