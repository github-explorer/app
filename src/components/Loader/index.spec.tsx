import { shallow } from 'enzyme';

import { Loader } from './index';

describe('Search input test', () => {
  it('should output a loading spinner', () => {
    const wrapper = shallow(<Loader />)
    expect(wrapper.find('FaCircleNotch')).toHaveLength(1);
  });
})