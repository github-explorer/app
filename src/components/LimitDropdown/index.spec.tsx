import { shallow, mount } from 'enzyme';

import { LimitDropdown } from './index';

const TEST_VALUE = 10;

const mockOnChange = jest.fn();

describe('Search input test', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = shallow(<LimitDropdown value={TEST_VALUE} onChange={mockOnChange} />)
  });

  it('should output a dropdown button by default', () => {
    expect(wrapper.find('button')).toHaveLength(1);
  });

  it('should render the default value', () => {
    wrapper = mount(<LimitDropdown value={TEST_VALUE} onChange={mockOnChange} />)

    expect(wrapper.find('button').text()).toBe(`Limit: ${TEST_VALUE}`);
  });

  it('should hide dropdown menu by default', () => {
    wrapper = mount(<LimitDropdown value={TEST_VALUE} onChange={mockOnChange} />)
    expect(wrapper.find({ "data-testid": "dropdown-menu" })).toHaveLength(0);
  });

  it('should render menu when clicked on dropdown button', () => {
    wrapper = mount(<LimitDropdown value={TEST_VALUE} onChange={mockOnChange} />)
    wrapper.find('button').simulate('click');
    expect(wrapper.find({ "data-testid": "dropdown-menu" })).toHaveLength(1);
  });
})