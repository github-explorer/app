import { useEffect, useState } from 'react';

import { LimitDropdown } from 'src/components/LimitDropdown';

export type PaginationPropTypes = {
  page: number;
  limit: number;
  onPageChange: (page: number) => void;
  onLimitChange: (limit: number) => void;
  total: number;
};

export const Pagination = ({
  page = 0,
  limit = 10,
  onPageChange,
  onLimitChange,
  total,
}: PaginationPropTypes) => {
  const totalPages = Math.ceil(total / limit);
  const [pagesToRender, setPagesToRender] = useState<number[]>([]);

  const changePage = (newPage: number) => {
    if (newPage !== page) {
      if (newPage <= totalPages && newPage > 0) {
        onPageChange(newPage);
      }
    }
  };

  useEffect(() => {
    let checkMark = page - 4;
    let newPagesToRender = [];

    while (pagesToRender.length < totalPages && newPagesToRender.length < 6) {
      if (checkMark > totalPages) {
        checkMark = 0;
      }
      checkMark = checkMark + 1;
      if (checkMark > 0 && checkMark <= totalPages) {
        newPagesToRender.push(checkMark);
      }
    }

    setPagesToRender(newPagesToRender);
  }, [page, pagesToRender.length, totalPages]);

  const sortedPages = [...pagesToRender].sort();
  const nextPage = sortedPages[sortedPages.length - 1] + 1;

  return (
    <div className="mt-4 flex justify-end">
      <LimitDropdown value={limit} onChange={onLimitChange} />

      <nav
        className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px ml-2"
        aria-label="Pagination"
      >
        {page > 1 && (
          <button
            onClick={() => changePage(page - 1)}
            className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"
          >
            <span className="sr-only">Previous</span>
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        )}
        {sortedPages.map((sp) => {
          return (
            <button
              key={`${page}-${sp}`}
              onClick={() => changePage(sp)}
              className={`${
                page === sp
                  ? 'bg-teal-100 text-teal-600'
                  : 'bg-white text-gray-500 hover:bg-gray-50'
              } relative inline-flex items-center px-4 py-2 border text-sm font-medium`}
            >
              {sp}
            </button>
          );
        })}
        {totalPages > page && (
          <button
            onClick={() => changePage(nextPage)}
            className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"
          >
            <span className="sr-only">Next</span>
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        )}
      </nav>
    </div>
  );
};
