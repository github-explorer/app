import { Suspense } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Loader from 'src/components/Loader';
import PublicRoutes from 'src/routes/public';

const Root = () => {
  return (
    <Router>
      <Suspense fallback={<Loader />}>
        <PublicRoutes />
      </Suspense>
    </Router>
  );
};

export default Root;
