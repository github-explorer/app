import { RiGitRepositoryLine } from 'react-icons/ri';
import { AiOutlineStar } from 'react-icons/ai';
import { Link } from 'react-router-dom';

import { RepositoryType } from 'src/types';
import { SortDropdown } from 'src/components/SortDropdown';

import { SortOptionType } from 'src/pages/Search/types';
import Loader from '../Loader';

export type SearchResultsPropTypes = {
  loading?: boolean;
  onSortChange?: (sortOption: SortOptionType) => void;
  repositories: RepositoryType[];
  selectedSortOption: SortOptionType;
  sortOptions: SortOptionType[];
  total: number;
};

export const SearchResults = ({
  onSortChange,
  repositories,
  selectedSortOption,
  sortOptions,
  total,
  loading = false,
}: SearchResultsPropTypes) => {
  const renderRepositories = () => {
    if (repositories && repositories.length) {
      return (
        <ul>
          {repositories.map((repository) => (
            <li
              key={repository.id}
              className="flex justify-between flex-wrap items-center bg-white mt-2 p-2 rounded transition"
            >
              <div className="flex ml-2 w-full">
                <div className="flex flex-col ml-2 w-full">
                  <div className="flex flex-row items-center">
                    <RiGitRepositoryLine className="mr-2" />
                    <Link
                      className="font-medium text-blue-500 hover:text-blue-700"
                      to={`/repository/${repository.full_name}`}
                    >
                      {repository.name}
                    </Link>
                  </div>
                  <span className="min-w-0 text-sm text-gray-500 truncate w-full overflow pr-5 mt-1">
                    {repository.description}
                  </span>
                  <div className="flex flex-row items-center text-gray-500 text-sm mt-1">
                    <AiOutlineStar className="mr-1" />
                    <span>{repository.watchers_count}</span>
                  </div>
                </div>
              </div>
            </li>
          ))}
        </ul>
      );
    }

    return (
      <div className="flex justify-center text-teal-800 text-lg font-bold">
        No repositories to show
      </div>
    );
  };

  return (
    <div className="flex flex-col justify-between h-full">
      {loading && <Loader />}
      <div className="flex justify-start flex-col h-full overflow-y-auto overflow-x-hidden">
        <div className="flex border-b-2 pb-3 mb-3 border-gray500 flex-col items-start md:flex-row md:items-center">
          <div className="flex justify-start w-full items-center">
            <h3 className="m-0 p-0">
              Showing
              <span className="text-teal-500 font-bold mx-2">
                {total.toLocaleString(undefined, { minimumFractionDigits: 0 })}
              </span>
              available repository results
            </h3>
          </div>
          <div className="flex justify-end mt-3 md:mt-0 md:w-full">
            <SortDropdown
              selectedOption={selectedSortOption}
              options={sortOptions}
              onSortChange={onSortChange}
            />
          </div>
        </div>

        {renderRepositories()}
      </div>
    </div>
  );
};
