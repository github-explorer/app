import ReactMarkdown from 'react-markdown';

import { RepositoryDetailType, RepositoryReadmeType } from 'src/types';

export type RepositoryDetailPropTypes = {
  repository: RepositoryDetailType | null;
  readme: RepositoryReadmeType | null;
  loading: boolean;
  readmeLoading: boolean;
};

export const RepositoryDetail = ({
  repository,
  readme,
  loading,
  readmeLoading,
}: RepositoryDetailPropTypes) => {
  if (loading || !repository) {
    return null;
  }

  const renderReadme = () => {
    if (readmeLoading) {
      return <span>Readme Loading....</span>;
    }

    if (!readme) {
      return null;
    }

    const markdown: string =
      Buffer.from(readme.content as any, 'base64').toString('utf8') || '';

    return (
      <div className="mt-10 py-10 border-t border-gray-200 text-center h-full">
        <div className="flex flex-wrap justify-center h-full">
          <div className="w-full h-full markdown-body w-full bg-transparent">
            <ReactMarkdown children={markdown} />
          </div>
        </div>
      </div>
    );
  };

  return (
    <main className="detail-page h-full">
      <div className="container py-2 mx-auto px-4 h-full">
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 h-full">
          <div className="px-6 h-full">
            <div className="flex flex-wrap justify-center">
              <div className="w-full flex justify-center relative">
                <img
                  alt={repository.owner.login}
                  src={repository.owner.avatar_url}
                  height={70}
                  width={70}
                  className="shadow-xl rounded-full align-middle border-none absolute -mt-20"
                />
              </div>
              <div className="w-full lg:w-4/12 px-4 flex justify-center lg:order-3 lg:text-right lg:self-center">
                <div className="py-6 px-3 sm:mt-12 md:mt-0">
                  <a
                    className="bg-teal-500 active:bg-teal-600 uppercase text-white font-bold hover:shadow-md shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2 mb-1 ease-linear transition-all duration-150"
                    href={repository.html_url}
                    rel="noreferrer"
                    target="_blank"
                    type="button"
                  >
                    View on github
                  </a>
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4 lg:order-1">
                <div className="flex justify-center py-4 lg:pt-4 pt-8">
                  <div className="mr-4 p-3 text-center">
                    <span className="text-xl font-bold block uppercase tracking-wide text-gray-600">
                      {repository.open_issues_count}
                    </span>
                    <span className="text-sm text-gray-400">Open Issues</span>
                  </div>
                  <div className="mr-4 p-3 text-center">
                    <span className="text-xl font-bold block uppercase tracking-wide text-gray-600">
                      {repository.forks_count}
                    </span>
                    <span className="text-sm text-gray-400">Forks</span>
                  </div>
                  <div className="lg:mr-4 p-3 text-center">
                    <span className="text-xl font-bold block uppercase tracking-wide text-gray-600">
                      {repository.watchers_count}
                    </span>
                    <span className="text-sm text-gray-400">Stars</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="text-center mt-12">
              <h3 className="text-4xl font-semibold leading-normal mb-2 text-gray-700 mb-2">
                {repository.full_name}
              </h3>
              <a
                href={repository.owner.html_url}
                target="_blank"
                className="text-sm leading-normal mt-0 mb-2 text-blue-400 cursor-pointer hover:text-blue-500 font-bold uppercase"
                rel="noreferrer"
              >
                {repository.owner.login}
              </a>
              <div className="mb-2 text-gray-600 mt-10">
                Default branch:{' '}
                <span className="font-bold">{repository.default_branch}</span>
              </div>
            </div>
            {renderReadme()}
          </div>
        </div>
      </div>
    </main>
  );
};
