import { shallow, mount } from 'enzyme';

import { SearchInput } from './index';

const TEST_VALUE = 'test';
const CHANGED_VALUE = 'test-modified';

const mockOnChange = jest.fn();
const mockOnSearch = jest.fn();

describe('Search input test', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = shallow(<SearchInput query={TEST_VALUE} onChange={mockOnChange} onSearch={mockOnSearch} />)
  });

  it('should output an input for search', () => {
    expect(wrapper.find('input')).toHaveLength(1);
  });

  it('should output an button for submit', () => {
    expect(wrapper.find('button')).toHaveLength(1);
  });

  it('should render the default value', () => {
    const wrapper = mount(<SearchInput query={TEST_VALUE} onChange={mockOnChange} onSearch={mockOnSearch} />);
    const input = wrapper.find('input');

    expect(input.get(0).props.value).toEqual(TEST_VALUE);
  });

  it('should call onChange when value is changed', () => {
    const input = wrapper.find('input');

    input.simulate('change', { target: { value: CHANGED_VALUE } });

    expect(mockOnChange.mock.calls.length).toEqual(1);
  });

  it('should provide changed value to onChange method', () => {
    const input = wrapper.find('input');

    input.simulate('change', { target: { value: CHANGED_VALUE } });

    expect(mockOnChange.mock.calls[0][0]).toBe(CHANGED_VALUE);
  });

  it('should call onSerach when clicked on the button', () => {
    const button = wrapper.find('button');

    button.simulate('click');

    expect(mockOnSearch.mock.calls.length).toEqual(1);
  });
})