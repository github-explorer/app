import { RepositoryDetailType, RepositoryReadmeType } from 'src/types';

export const REPOSITORY = {
  FETCH_DATA: 'REPOSITORY_FETCH_DATA' as const,
  FETCH_DATA_SUCCEEDED: 'REPOSITORY_FETCH_DATA_SUCCEEDED' as const,
  FETCH_DATA_FAILED: 'REPOSITORY_FETCH_DATA_FAILED' as const,

  FETCH_README_DATA: 'REPOSITORY_FETCH_README_DATA' as const,
  FETCH_README_DATA_SUCCEEDED:
    'REPOSITORY_FETCH_README_DATA_SUCCEEDED' as const,
  FETCH_README_DATA_FAILED: 'REPOSITORY_FETCH_README_DATA_FAILED' as const,
};

export type RepositoryStoreType = {
  error: null | string;
  errorReadme: null | string;
  repository: null | RepositoryDetailType;
  readme: null | RepositoryReadmeType;
  fetchRepositoryPending: boolean;
  fetchRepositoryReadmePending: boolean;
};

export interface FetchDataParamTypes {
  repositoryUser: string;
  repositoryName: string;
}

interface FetchDataAction {
  type: typeof REPOSITORY.FETCH_DATA;
  data: FetchDataParamTypes;
}

interface FetchDataSucceededAction {
  type: typeof REPOSITORY.FETCH_DATA_SUCCEEDED;
  repository: RepositoryDetailType;
}

interface FetchDataFailedAction {
  type: typeof REPOSITORY.FETCH_DATA_FAILED;
  error: string;
}

interface FetchReadmeDataAction {
  type: typeof REPOSITORY.FETCH_README_DATA;
  data: FetchDataParamTypes;
}

interface FetchReadmeDataSucceededAction {
  type: typeof REPOSITORY.FETCH_README_DATA_SUCCEEDED;
  readme: RepositoryReadmeType;
}

interface FetchReadmeDataFailedAction {
  type: typeof REPOSITORY.FETCH_README_DATA_FAILED;
  error: string;
}

export type RepositoryActionTypes =
  | FetchDataAction
  | FetchDataSucceededAction
  | FetchDataFailedAction
  | FetchReadmeDataAction
  | FetchReadmeDataSucceededAction
  | FetchReadmeDataFailedAction;
