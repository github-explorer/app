import { call, put } from 'redux-saga/effects';

import { RepositoryDetailType, RepositoryReadmeType } from 'src/types';

import { FetchDataParamTypes } from './types';
import { getRepositoryDetail, getRepositoryReadme } from './api';
import {
  fetchRepositoryFailed,
  fetchRepositoryReadmeFailed,
  fetchRepositoryReadmeSucceeded,
  fetchRepositorySucceeded,
} from './actions';

type handleFetchRepositoryParamTypes = {
  type: string;
  data: FetchDataParamTypes;
};

export function* handleFetchRepository(
  params: handleFetchRepositoryParamTypes,
) {
  try {
    const { repository } = yield call(getRepositoryDetail, params.data);
    yield put(fetchRepositorySucceeded(repository as RepositoryDetailType));
  } catch (e) {
    yield put(fetchRepositoryFailed(e as string));
  }
}

export function* handleFetchRepositoryReadme(
  params: handleFetchRepositoryParamTypes,
) {
  try {
    const { readme } = yield call(getRepositoryReadme, params.data);
    yield put(fetchRepositoryReadmeSucceeded(readme as RepositoryReadmeType));
  } catch (e) {
    yield put(fetchRepositoryReadmeFailed(e as string));
  }
}
