import {
  REPOSITORY,
  RepositoryActionTypes,
  RepositoryStoreType,
} from './types';

export const initialState: RepositoryStoreType = {
  error: null,
  errorReadme: null,
  repository: null,
  readme: null,
  fetchRepositoryPending: false,
  fetchRepositoryReadmePending: false,
};

export const searchReducer = (
  state = initialState,
  action: RepositoryActionTypes,
) => {
  switch (action.type) {
    case REPOSITORY.FETCH_DATA:
      return {
        ...state,
        fetchRepositoryPending: true,
      };
    case REPOSITORY.FETCH_DATA_SUCCEEDED:
      return {
        ...state,
        repository: action.repository,
        error: null,
        fetchRepositoryPending: false,
      };
    case REPOSITORY.FETCH_DATA_FAILED:
      return {
        ...state,
        error: action.error,
        fetchRepositoryPending: false,
      };

    case REPOSITORY.FETCH_README_DATA:
      return {
        ...state,
        fetchRepositoryReadmePending: true,
      };
    case REPOSITORY.FETCH_README_DATA_SUCCEEDED:
      return {
        ...state,
        readme: action.readme,
        errorReadme: null,
        fetchRepositoryReadmePending: false,
      };
    case REPOSITORY.FETCH_README_DATA_FAILED:
      return {
        ...state,
        errorReadme: action.error,
        fetchRepositoryReadmePending: false,
      };
    default:
      return state;
  }
};

export default searchReducer;
