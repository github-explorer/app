import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import {
  fetchRepository,
  fetchRepositoryReadme,
} from 'src/pages/Repository/actions';
import { Store } from 'src/store/types';
import { RepositoryDetail } from 'src/components/RepositoryDetail';

export const RepositoryPage = () => {
  const dispatch = useDispatch();
  const { repositoryUser, repositoryName } = useParams();
  const {
    repository,
    readme,
    fetchRepositoryPending,
    fetchRepositoryReadmePending,
  } = useSelector((state: Store) => ({
    repository: state.repository.repository,
    readme: state.repository.readme,
    fetchRepositoryPending: state.repository.fetchRepositoryPending,
    fetchRepositoryReadmePending: state.repository.fetchRepositoryReadmePending,
  }));

  useEffect(() => {
    if (repositoryUser && repositoryName) {
      dispatch(
        fetchRepository({
          repositoryUser,
          repositoryName,
        }),
      );
      dispatch(
        fetchRepositoryReadme({
          repositoryUser,
          repositoryName,
        }),
      );
    }
  }, [dispatch, repositoryName, repositoryUser]);

  return (
    <div
      className={`py-10 h-full bg-gray-300 px-2 ${
        fetchRepositoryReadmePending ? 'h-screen' : 'h-full'
      }`}
    >
      <div className="max-w-md mx-auto bg-gray-100 shadow-lg rounded-lg overflow-hidden md:max-w-5xl h-full">
        <div className="md:flex h-full py-16">
          <div className="w-full h-full p-4">
            <RepositoryDetail
              loading={fetchRepositoryPending}
              readme={readme}
              readmeLoading={fetchRepositoryReadmePending}
              repository={repository}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default RepositoryPage;
