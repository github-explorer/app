import { RepositoryDetailType, RepositoryReadmeType } from 'src/types';

import {
  REPOSITORY,
  FetchDataParamTypes,
  RepositoryActionTypes,
} from './types';

export function fetchRepository({
  repositoryUser,
  repositoryName,
}: FetchDataParamTypes): RepositoryActionTypes {
  return {
    type: REPOSITORY.FETCH_DATA,
    data: {
      repositoryUser,
      repositoryName,
    },
  };
}

export function fetchRepositorySucceeded(
  repository: RepositoryDetailType,
): RepositoryActionTypes {
  return {
    type: REPOSITORY.FETCH_DATA_SUCCEEDED,
    repository,
  };
}

export function fetchRepositoryFailed(error: string): RepositoryActionTypes {
  return {
    type: REPOSITORY.FETCH_DATA_FAILED,
    error,
  };
}

export function fetchRepositoryReadme({
  repositoryUser,
  repositoryName,
}: FetchDataParamTypes): RepositoryActionTypes {
  return {
    type: REPOSITORY.FETCH_README_DATA,
    data: {
      repositoryUser,
      repositoryName,
    },
  };
}

export function fetchRepositoryReadmeSucceeded(
  readme: RepositoryReadmeType,
): RepositoryActionTypes {
  return {
    type: REPOSITORY.FETCH_README_DATA_SUCCEEDED,
    readme,
  };
}

export function fetchRepositoryReadmeFailed(
  error: string,
): RepositoryActionTypes {
  return {
    type: REPOSITORY.FETCH_README_DATA_FAILED,
    error,
  };
}
