import axiosClient from 'src/axiosClient';

import { FetchDataParamTypes } from './types';

export const getRepositoryDetail = ({
  repositoryUser,
  repositoryName,
}: FetchDataParamTypes) =>
  new Promise((resolve, reject) => {
    axiosClient
      .get(`/repository/detail/${repositoryUser}/${repositoryName}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error?.response?.data?.message) {
          reject(error.response.data.message);
        } else {
          reject(error.message);
        }
      });
  });

export const getRepositoryReadme = ({
  repositoryUser,
  repositoryName,
}: FetchDataParamTypes) =>
  new Promise((resolve, reject) => {
    axiosClient
      .get(`/repository/detail/${repositoryUser}/${repositoryName}/readme`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error?.response?.data?.message) {
          reject(error.response.data.message);
        } else {
          reject(error.message);
        }
      });
  });
