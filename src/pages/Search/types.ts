import { RepositoryType } from 'src/types';

export const SEARCH = {
  FETCH_DATA: 'SEARCH_FETCH_DATA' as const,
  SEARCH_SUCCEEDED: 'SEARCH_FETCH_DATA_SUCCEEDED' as const,
  SEARCH_FAILED: 'SEARCH_FETCH_DATA_FAILED' as const,
};

export type SortOptionType = {
  label: string;
  sort: string;
  order: 'desc' | 'asc';
};

export type SearchStoreType = {
  error: null;
  repositories: RepositoryType[];
  searchRepositoriesPending: boolean;
  totalRepositories: number;
};

export interface SearchParamTypes {
  q: string;
  sort: string;
  order: string;
  page: number;
  limit: number;
}

interface FetchDataAction {
  type: typeof SEARCH.FETCH_DATA;
  data: SearchParamTypes;
}

interface SearchSucceededAction {
  type: typeof SEARCH.SEARCH_SUCCEEDED;
  repositories: RepositoryType[];
  total: number;
}

interface SearchFailedAction {
  type: typeof SEARCH.SEARCH_FAILED;
  error: string;
}

export type SearchActionTypes =
  | FetchDataAction
  | SearchSucceededAction
  | SearchFailedAction;
