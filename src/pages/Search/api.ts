import axiosClient from 'src/axiosClient';

import { SearchParamTypes } from './types';

export const searchRepositories = ({
  q,
  sort,
  order,
  page,
  limit,
}: SearchParamTypes) =>
  new Promise((resolve, reject) => {
    const urlSearchParams = new URLSearchParams({
      q,
      sort,
      order,
      page: `${page}`,
      limit: `${limit}`,
    }).toString();

    axiosClient
      .get(`/search?${urlSearchParams}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error?.response?.data?.message) {
          reject(error.response.data.message);
        } else {
          reject(error.message);
        }
      });
  });
