import { call, put } from 'redux-saga/effects';

import { RepositoryType } from 'src/types';

import { SearchParamTypes } from './types';
import { searchRepositories } from './api';
import {
  searchRepositoriesFailed,
  searchRepositoriesSucceeded,
} from './actions';

type handleSearchRepositoriesParamTypes = {
  type: string;
  data: SearchParamTypes;
};

export function* handleSearchRepositories(
  params: handleSearchRepositoriesParamTypes,
) {
  try {
    const {
      data: { repositories, total },
    } = yield call(searchRepositories, params.data);
    yield put(
      searchRepositoriesSucceeded(
        repositories as RepositoryType[],
        total as number,
      ),
    );
  } catch (e) {
    yield put(searchRepositoriesFailed(e as string));
  }
}
