import { SEARCH, SearchActionTypes, SearchStoreType } from './types';

export const initialState: SearchStoreType = {
  error: null,
  repositories: [],
  totalRepositories: 0,
  searchRepositoriesPending: false,
};

export const searchReducer = (
  state = initialState,
  action: SearchActionTypes,
) => {
  switch (action.type) {
    case SEARCH.FETCH_DATA:
      return {
        ...state,
        searchRepositoriesPending: true,
      };
    case SEARCH.SEARCH_SUCCEEDED:
      return {
        ...state,
        repositories: action.repositories,
        totalRepositories: action.total,
        searchRepositoriesPending: false,
      };
    case SEARCH.SEARCH_FAILED:
      return {
        ...state,
        error: action.error,
        searchRepositoriesPending: false,
      };
    default:
      return state;
  }
};

export default searchReducer;
