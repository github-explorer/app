import { RepositoryType } from 'src/types';

import { SEARCH, SearchActionTypes, SearchParamTypes } from './types';

export function fetchRepositories({
  q,
  sort,
  order,
  page,
  limit,
}: SearchParamTypes): SearchActionTypes {
  return {
    type: SEARCH.FETCH_DATA,
    data: {
      q,
      sort,
      order,
      page,
      limit,
    },
  };
}

export function searchRepositoriesSucceeded(
  repositories: RepositoryType[],
  total: number,
) {
  return {
    type: SEARCH.SEARCH_SUCCEEDED,
    repositories,
    total,
  };
}

export function searchRepositoriesFailed(error: string) {
  return {
    type: SEARCH.SEARCH_FAILED,
    error,
  };
}
