import { useDispatch, useSelector } from 'react-redux';
import { useState, useCallback } from 'react';

import { SearchInput } from 'src/components/SearchInput';
import { SearchResults } from 'src/components/SearchResults';
import { Pagination } from 'src/components/Pagination';
import { fetchRepositories } from 'src/pages/Search/actions';
import { Store } from 'src/store/types';

import { SortOptionType } from '../types';

const sortOptions: Array<SortOptionType> = [
  {
    label: 'Best match',
    sort: 'best-match',
    order: 'desc',
  },
  {
    label: 'Most stars',
    sort: 'stars',
    order: 'desc',
  },
  {
    label: 'Fewest stars',
    sort: 'stars',
    order: 'asc',
  },
  {
    label: 'Most forks',
    sort: 'forks',
    order: 'desc',
  },
  {
    label: 'Fewest forks',
    sort: 'forks',
    order: 'asc',
  },
  {
    label: 'Recently updated',
    sort: 'updated',
    order: 'desc',
  },
  {
    label: 'Least recently updated',
    sort: 'updated',
    order: 'asc',
  },
];

type onSearchParamTypes = {
  sort?: string;
  order?: 'asc' | 'desc';
  query?: string;
  limit?: number;
  page?: number;
};

export const SearchPage = () => {
  const [query, setQuery] = useState('');
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [selectedOption, setSelectedOption] = useState<SortOptionType>(
    sortOptions[0],
  );
  const dispatch = useDispatch();
  const { repositories, searchRepositoriesPending, totalRepositories } =
    useSelector((state: Store) => ({
      repositories: state.search.repositories,
      searchRepositoriesPending: state.search.searchRepositoriesPending,
      totalRepositories: state.search.totalRepositories,
    }));

  const onSearch = useCallback(
    (searchOptions?: onSearchParamTypes) => {
      const { sort, order } = selectedOption;
      const newSearchOptions = {
        ...{
          q: query,
          sort,
          order,
          page,
          limit,
        },
        ...searchOptions,
      };
      if (newSearchOptions.q && newSearchOptions.q.length) {
        dispatch(fetchRepositories(newSearchOptions));
      }
    },
    [dispatch, limit, page, query, selectedOption],
  );

  const onSortChange = useCallback(
    (sortOption: SortOptionType) => {
      if (sortOption.label !== selectedOption.label) {
        setSelectedOption(sortOption);
        if (query && query.length) {
          onSearch({ sort: sortOption.sort, order: sortOption.order });
        }
      }
    },
    [onSearch, query, selectedOption.label],
  );

  const onPageChange = useCallback(
    (newPage: number) => {
      if (newPage !== page) {
        setPage(newPage);
        if (query && query.length) {
          onSearch({ page: newPage });
        }
      }
    },
    [onSearch, page, query],
  );

  const onLimitChange = useCallback(
    (newLimit: number) => {
      if (newLimit !== limit) {
        setLimit(newLimit);
        if (query && query.length) {
          onSearch({ limit: newLimit });
        }
      }
    },
    [limit, onSearch, query],
  );

  return (
    <div
      className={`py-10 bg-gray-300 px-2 ${
        repositories && repositories.length > 5 ? 'h-full' : 'h-screen'
      }`}
    >
      <div className="max-w-md mx-auto bg-gray-100 shadow-lg rounded-lg md:max-w-5xl h-full mb-32">
        <div className="flex flex-col h-full pt-16">
          <div className="mb-4 flex justify-center">
            <h1 className="font-bold text-3xl text-teal-600">
              Github Explorer
            </h1>
          </div>
          <div className="w-full h-full p-4 flex flex-col">
            <SearchInput
              query={query}
              onChange={(value: string) => setQuery(value)}
              onSearch={() => onSearch()}
            />

            <SearchResults
              sortOptions={sortOptions}
              onSortChange={onSortChange}
              repositories={repositories}
              selectedSortOption={selectedOption}
              total={totalRepositories}
              loading={searchRepositoriesPending}
            />

            {totalRepositories && totalRepositories > 10 ? (
              <Pagination
                limit={limit}
                onLimitChange={onLimitChange}
                onPageChange={onPageChange}
                page={page}
                total={totalRepositories}
              />
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchPage;
