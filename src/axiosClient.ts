import axios from 'axios';

const host = process.env.REACT_APP_API_URL;

const axiosInstance = axios.create({
  baseURL: host,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export default axiosInstance;
