export type RepositoryType = {
  description: string;
  forks_count: number;
  full_name: string;
  id: number;
  name: string;
  open_issues_count: number;
  watchers_count: number;
};

export type RepositoryDetailType = {
  forks_count: number;
  full_name: string;
  description: string;
  html_url: string;
  open_issues_count: number;
  default_branch: string;
  owner: {
    avatar_url: string;
    login: string;
    html_url: string;
  };
  watchers_count: number;
};

export type RepositoryReadmeType = {
  content: string;
  encoding: string;
};

export type SortType = {
  sort: string;
  order: 'asc' | 'desc';
};
