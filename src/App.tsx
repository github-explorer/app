import { Provider } from 'react-redux';

import Root from 'src/components/Root';

import configureStore from './store';
import 'src/assets/scss/index.scss';

const { store } = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <div id="app">
        <Root />
      </div>
    </Provider>
  );
};

export default App;
