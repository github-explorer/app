import { lazy } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import { PATHS } from './paths';

const RepositoryPage = lazy(() => import('src/pages/Repository/containers'));
const SearchPage = lazy(() => import('src/pages/Search/containers'));

const PublicRoutes = () => {
  return (
    <Routes>
      <Route path={PATHS.ROOT} element={<SearchPage />} />
      <Route path={PATHS.REPOSITORY_VIEW} element={<RepositoryPage />} />
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default PublicRoutes;
