export const PATHS = {
  ROOT: '/',
  REPOSITORY_VIEW: '/repository/:repositoryUser/:repositoryName',
};
