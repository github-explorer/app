import tailwindConfigs from './tailwind.config';

export const configs = {
  style: {
    postcss: {
      plugins: [
        require('tailwindcss')(tailwindConfigs),
        require('autoprefixer'),
      ],
    },
  },
};

export default configs;