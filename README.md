# Github Explorer - React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instalation

Be sure to have [node](https://nodejs.org/en/), [npm](https://www.npmjs.com/), [yarn](https://yarnpkg.com) installed.
We use yarn (and yarn.lock) as default but you should be able to use npm.
To handle multiple node version using [nvm](https://github.com/creationix/nvm) check the package.json for the current node version or the nvmrc file.


## Setup
---
* clone the repo

  ```
    git clone git@gitlab.com:github-explorer/app.git
  ```

* Go inside the app

  ```
    cd app
  ```
* Setup environment file
  ```
    cp .env.example .env
  ```

* Install packages

  ```
    yarn
  ```

* Start

  ```
    yarn start
  ```


---
## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Improvements
* some E2E tests with the API
* more detailed tests after the components changes states
* some components like dropdown could be made generic
* the pagination generation can be improved (react fails to rerender when some of pagination items use same number to render)
* with composition api all of the redux and saga need could be removed...
